#include <cmath>
#include <iostream>
#include <unistd.h>
#include <algorithm>

#include <ros/ros.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/Odometry.h>
#include <ackermann_msgs/AckermannDriveStamped.h>

#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>

static const std::string OPENCV_WINDOW = "Camera from Baggy";

class SimpleMover {

    ros::NodeHandle nh;
    ros::Publisher cmd_vel_pub;
    ros::Subscriber image_sub;
    ros::Subscriber sonar_sub;
    ros::Subscriber lidar_sub;
    ros::Subscriber gps_sub;
    ros::Rate rate = ros::Rate(30);
    bool gui;
    cv_bridge::CvImagePtr cv_ptr;

    std::array<double, 3> *gps_coord;
    double sonar_range;



  public:

    SimpleMover() {


        image_sub = nh.subscribe("/robot/robot_front_ptz_camera/image_raw", 1, &SimpleMover::camera_cb, this);
        gps_sub = nh.subscribe("/robot/odometry/gps", 1, &SimpleMover::gps_cb, this);
        lidar_sub = nh.subscribe("/robot/lidar_3d/points", 1, &SimpleMover::lidar_cb, this);
        sonar_sub = nh.subscribe("/robot/teraranger_duo/sonar", 1, &SimpleMover::sonar_cb, this);
        cmd_vel_pub = nh.advertise<ackermann_msgs::AckermannDriveStamped>("/robot/robot_cmd_vel", 1);
        ros::param::get("/gui", gui);
        cv::namedWindow(OPENCV_WINDOW);

        this->gps_coord = new std::array<double, 3>;
        this->sonar_range = 0.0;

        ros::Duration(1).sleep();       // требуется для инициализации времени
    }                                   // при слишком быстром старте узла


    ~SimpleMover() {
        cv::destroyWindow(OPENCV_WINDOW);
        delete this->gps_coord;
    }

    void camera_cb(const sensor_msgs::Image::ConstPtr &msg) {
        try {
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        } catch (cv_bridge::Exception& e) {
            ROS_ERROR("cv_bridge exception: %s", e.what());
            return;
        }
        if (gui == true)
        {
            show_image(cv_ptr);
        }
    }

    // обновление координат робота
    void gps_cb(const nav_msgs::Odometry::ConstPtr &msg){
        this->gps_coord->at(0) = msg->pose.pose.position.x;
        this->gps_coord->at(1) = msg->pose.pose.position.y;
        this->gps_coord->at(2) = msg->pose.pose.position.z;
    }

    // обновление значения лидара
    void lidar_cb(const sensor_msgs::PointCloud2::ConstPtr &msg){
        std::cout << "Advertise to lidar. Is dense? " << msg->is_dense << std::endl;
    }

    void sonar_cb(const sensor_msgs::Range::ConstPtr &msg){
        this->sonar_range = msg->range;
    }

    void show_image(const cv_bridge::CvImagePtr cv_ptr) {
        cv::imshow(OPENCV_WINDOW, cv_ptr->image);
        cv::waitKey(3);
    }

    ackermann_msgs::AckermannDriveStamped create_ackermann_msgs(std::string const &frame, double steering_angle, double steering_angle_vel,
    double speed, double acceleration){
        ackermann_msgs::AckermannDriveStamped msg;
        msg.header.frame_id = frame;
        msg.drive.speed = speed;
        msg.drive.acceleration = acceleration;
        msg.drive.steering_angle = steering_angle;
        msg.drive.steering_angle_velocity = steering_angle_vel;
        return msg;
    }


    void go_forward(){
        ros::spinOnce();
        rate.sleep();
        std::cout << "go forward" << std::endl;
        std::cout << "sonar range: " << this->sonar_range << std::endl;
        while (this->sonar_range > 4.0){
            double speed = 0.0;
            double acceleration = 0.0;
            std::cout << "sonar range: " << this->sonar_range << std::endl;
            std::cout << "coord x: " << this->gps_coord->at(0) << " y: " << this->gps_coord->at(1) << " z: " << this->gps_coord->at(2) << std::endl;
            if (this->sonar_range > 5){
                speed = 10;
                acceleration = 0.0;
            }else{
                speed = 0;
                acceleration = -5;
            }
            auto msg = create_ackermann_msgs("robot_odom", 0.0, 0.0, speed, acceleration);
            cmd_vel_pub.publish(msg);
            ros::spinOnce();
            rate.sleep();
        }
    }

    void turn_right(){
        std::cout << "turn right" << std::endl;
        for (int i = 0; i < 10; i++){ // поворачиваем на 1 градус
            auto msg = create_ackermann_msgs("robot_odom", -9*3.14/180, 0.0, 1, 0.0);
            cmd_vel_pub.publish(msg);
            ros::spinOnce();
            rate.sleep();
        }
    }

    void test(){
        while (nh.ok()){
            go_forward();
            turn_right();
        }
    }
};

int main(int argc, char **argv) {

    ros::init(argc, argv, "simple_mover");

    SimpleMover simpleMover;
    simpleMover.test();

  return 0;
}
